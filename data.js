class NhanVien {
    constructor(id, name, position, office, age, startday){
        this.id = id;
        this.name =name;
        this.position =position;
        this.age = age;
        this.office = office;
        this.startday = startday;
    }
}
let NhanVien_1 = new NhanVien(1, 'Airi Satou', ' Accountant', 'Tokyo', '33', '2008/11/28');
let NhanVien_2 = new NhanVien(2, 'Angelica Ramos', '  Chief Executive Officer (CEO)', 'London', '47', ' 2009/10/09');
let NhanVien_3 = new NhanVien(3, 'Ashton Cox', ' Junior Technical Author', 'Tokyo', '33', ' 2009/01/12');
let NhanVien_4 = new NhanVien(4, 'Bradley Greer', '  Software Engineer', 'London', '66', ' 2012/10/13');
let NhanVien_5 = new NhanVien(5, 'Brenden Wagner', ' Software Engineer', 'San Francisco', '28', ' 2011/06/07');
let NhanVien_6 = new NhanVien(6, 'Brielle Williamson', ' Integration Specialist', 'New York', '61', '2012/12/02');
let NhanVien_7 = new NhanVien(7, 'Bruno Nash', ' Software Engineer', 'London', '38', '2011/05/03');
let NhanVien_8 = new NhanVien(8, 'Caesar Vance', ' Pre-Sales Support', 'Tokyo', '21', '2011/12/12');
let NhanVien_9 = new NhanVien(9, 'Cara Stevens', ' Sales Assistant', ' New York', '46', '2011/12/06');
let NhanVien_10 = new NhanVien(10, 'Cedric Kelly', ' Senior Javascript Developer', 'Edinburgh', '22', ' 2012/03/29');
 userList = [NhanVien_1, NhanVien_2, NhanVien_3, NhanVien_4, NhanVien_5, NhanVien_6, NhanVien_7, NhanVien_8, NhanVien_9, NhanVien_10];

module.exports = userList;

 