const { request, response } = require('express');
const express = require('express');
const nhanvienMiddleware = require('../middleware/nhanvienMiddleware');
const userList = require('../../data');
const router = express.Router();

// get all user
router.get('/users', nhanvienMiddleware, (request, response) => {

    response.status(200).json(
        userList
    )

})
// get quire user
router.get('/users/search', nhanvienMiddleware, (request, response) => {
    let queryRequest = request.query;
    if (queryRequest.age = '') {
        response.status(200).json(userList);
    }
    else {
        var userList1 = [];
        var userAge = userList.filter(paramAge => queryRequest.age < paramAge.age);
        userList1.push(userAge);
        response.status(200).json(userList1);
    }
})
// get byid user
router.get('/users/:userId', nhanvienMiddleware, (request, response) => {
    let Id = request.params.userId;
    if (typeof Id !== 'number' && Id > 10) {
        return response.status(400).json(
            {
                status: "Error 400: Bad Request",
                message: `lỗi ${Id} không tồn tại`
            }
        )
    }
    else {
        let userId1;
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].id == Id) {
                userId1 = userList[i];
            }
        }
        return response.status(200).json(
            { userId1 }
        );
    }

})

// delete by Id
router.delete('/users/:userId', nhanvienMiddleware, (request, response) => {
    let id = request.params.userId;
    // kiểm tra dữ liệu
    if (typeof id !== 'number' && id > 10) {
        return response.status(400).json(
            {
                status: "Error 400: Bad Request",
                message: `lỗi ${id} không tồn tại`
            }
        )
    }
    // thao tác vơi cơ sở dữ liệu
    else {
        let userId1;
        for (var i = 0; i < userList.length; i++) {
            if (userList[i].id == id) {
                userId1 = userList[i];
            }
        }
        return response.status(200).json(
            {
                status: "Success: Delete course success",
                message: `Đã xóa user thành công`
            }
        );
    }
})
// create body
router.post('/users', nhanvienMiddleware, (request, response)=>{
    //thu thap dữ liêu
    let bodyRequest = request.body;
    //kiểm tra dữ liệu
    if(typeof bodyRequest.id !== 'number'){
        return response.status(400).json(
            {
                status: "Error 400: Bad Request",
                message: `lỗi  id phải là số tư 1 đến 10`
            }
        )
    }
    else {
        return response.status(201).json({
            status: "Success: users created",
        })
    }
})
// thêm mới sưa
router.put('/users/:id', nhanvienMiddleware, (request, response)=>{
    //thu thap dữ liêu
    let bodyRequest = request.body;
    let id =request.params.id;


    //kiểm tra dữ liệu
    if(typeof id !== 'number' && id > 10){
        return response.status(400).json(
            {
                status: "Error 400: Bad Request",
                message: `lỗi  ${id} không phù hợp`
            }
        )
    }
    else {
        return response.status(201).json({
            status: "Success: Update course success",
        })
    }
})






module.exports = router;