const { request, response } = require("express");

const nhanvienMiddleware = (request, response, next) =>{
    console.log(`Method: ${request.method} - URL: ${request.url} - Time: ${new Date()} `);
    next();
}
module.exports = nhanvienMiddleware;