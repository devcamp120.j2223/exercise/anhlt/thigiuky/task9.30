const express = require('express');
const router = require('./app/router/nhanVienRouter');
const app = new express();
const port = 8000;



 
// su dung body json
app.use(express.json())
// chay dk tieengs viet
app.use(express.urlencoded({
    urlencoded: true
}))
app.use('/', router);


app.listen(port, () =>{
    console.log("App chạy trên cổng: " + port);
})



